<?php
namespace App\Tests\QA\Endpoints\Specification;

use App\Libraries\Specification\Types\Object\ObjectType;
use App\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SingleLevelSpecificationMock extends ObjectType
{
    /**
     * @return array
     */
    public function structure()
    {
        return [
            'a' => new StringType(),
            'b' => new StringType()
        ];
    }
}