<?php
namespace App\Tests\QA\Endpoints\Specification;

use App\Libraries\Specification\Types\Collection\CollectionType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class CollectionSpecificationMock extends CollectionType
{
    /**
     * @return array
     */
    public function specification()
    {
        return function(){
            return new SpecificationMock();
        };
    }
}