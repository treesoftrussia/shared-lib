<?php
namespace App\Tests\QA\Endpoints\Specification;

use App\Libraries\Specification\Types\Collection\CollectionType;
use App\Libraries\Specification\Types\Custom\PhoneType;
use App\Libraries\Specification\Types\Object\ObjectType;
use App\Libraries\Specification\Types\Scalar\IntegerType;
use App\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class EntitySpecificationMock extends ObjectType
{
    /**
     * @return array
     */
    public function structure()
    {
        return [
            'id' => new IntegerType(),
            'name' => new StringType(),
            'description' => new PhoneType(),
            'entity' => new SubEntitySpecificationMock(),
            'collection' => new CollectionType(function(){
                return new IntegerType();
            })
        ];
    }
}