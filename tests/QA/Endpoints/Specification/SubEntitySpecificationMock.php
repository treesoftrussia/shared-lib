<?php
namespace App\Tests\QA\Endpoints\Specification;

use App\Libraries\Specification\Types\Object\ObjectType;
use App\Libraries\Specification\Types\Scalar\IntegerType;
use App\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SubEntitySpecificationMock extends ObjectType
{
    /**
     * @return array
     */
    public function structure()
    {
        return [
            'id' => new IntegerType(),
            'name' => new StringType(),
            'description' => new StringType()
        ];
    }
}