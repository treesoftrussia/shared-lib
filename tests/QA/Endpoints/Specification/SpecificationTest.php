<?php
namespace App\Tests\QA\Endpoints\Specification;

use App\Libraries\Kangaroo\Exceptions\RuntimeException;
use App\Libraries\Specification\AType;
use App\Libraries\Specification\Support\PaginationSpecificationFactory;
use App\Libraries\Specification\Support\SpecificationFactory;
use App\Libraries\Specification\Types\Collection\CollectionType;
use App\Libraries\Specification\Types\Object\ObjectType;
use App\Libraries\Specification\Types\Scalar\IntegerType;
use App\Libraries\Specification\Types\Scalar\StringType;
use App\Tests\Libraries\Processor\TranslatorMock;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Foundation\Application;
use Illuminate\Support\Collection;
use Illuminate\Validation\Factory as ValidationFactory;
use Illuminate\Contracts\Validation\Factory as FactoryContract;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SpecificationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    private $app;

    public function setUp()
    {
        parent::setUp();

        $this->app = new Application();

        $this->faker = Factory::create();
    }

    private function createContainer()
    {
        $container = new Container();
        $container->bind(FactoryContract::class, ValidationFactory::class);
        $container->bind(TranslatorInterface::class, TranslatorMock::class);

        Container::setInstance($container);
    }

    public function testResponseSpecificationTest()
    {
        $this->createContainer();

        $response = [
            'field' => [
                'string' => $this->faker->sentence(),
                'enum' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'string2' => $this->faker->sentence(),
                'array1' => [['a' => $this->faker->sentence(), 'b' => $this->faker->sentence()]],
                'array2' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                'array3' => [$this->faker->randomFloat(), $this->faker->randomFloat(), $this->faker->randomFloat()],
                'emptyArray' => [],
                'emptyArray2' => [$this->faker->randomFloat()],
                'bool' => true,
                'int' => $this->faker->randomNumber(),
                'phone' => '131232131'
            ]
        ];

        $specification = SpecificationFactory::make(SpecificationMock::class);


        if (!$specification->check($response)) {
            dd($specification->getErrorMessages());
        }

        $this->assertTrue($specification->check($response));
    }


    public function testSpecificationTest2()
    {
        $this->createContainer();

        $response = [
            'field' => [
                'string' => null
            ]
        ];

        $specification = SpecificationFactory::make(ObjectType::class, [
            [
                'field' => new ObjectType([
                    'string' => new StringType(true)
                ])
            ]
        ])->setSpecificationType(AType::RESPONSE_SPECIFICATION);

        if (!$specification->check($response)) {
            dd($specification->getErrorMessages());
        }


        $this->assertTrue($specification->check($response));
    }

    public function testSpecificationTest3()
    {
        $this->createContainer();

        $response = [
            'field' => [
                'string' => null
            ]
        ];

        $specification = SpecificationFactory::make(ObjectType::class, [
            [
                'field' => new ObjectType([
                    'string' => new StringType()
                ])
            ]
        ])->setSpecificationType(AType::RESPONSE_SPECIFICATION);

        $specification->check($response);

        $this->assertEquals([
            "field.string" => [
                "Value can not be null"
            ]
        ], $specification->getErrorMessages());
    }


    public function testSpecificationTest4()
    {
        $this->createContainer();

        $response = [
            'field' => [
                'string' => null
            ]
        ];

        $specification = SpecificationFactory::make(ObjectType::class, [
            [
                'field' => new ObjectType([
                    'string' => new StringType()
                ])
            ]
        ])->setSpecificationType(AType::REQUEST_SPECIFICATION);

        $specification->check($response);

        $this->assertEquals([
            "field.string" => [
                "Value can not be null"
            ]
        ], $specification->getErrorMessages());
    }


    public function testSpecificationTest5()
    {
        $this->createContainer();

        $response = [
            'field' => [
                'a' => '!'
            ]
        ];

        $specification = SpecificationFactory::make(ObjectType::class, [
            [
                'field' => new ObjectType([
                    'a' => new StringType(),
                    'string' => new StringType(false, true)
                ])
            ]
        ])->setSpecificationType(AType::REQUEST_SPECIFICATION);

        $this->assertTrue($specification->check($response));
    }

    public function testSpecificationTest6()
    {
        $this->setExpectedException(RuntimeException::class, 'Optional types are not allowed for RESPONSE specifications.');

        $this->createContainer();

        $response = [
            'field' => [
                'a' => '!'
            ]
        ];

        $specification = SpecificationFactory::make(ObjectType::class, [
            [
                'field' => new ObjectType([
                    'a' => new StringType(),
                    'string' => new StringType(false, true)
                ])
            ]
        ])->setSpecificationType(AType::RESPONSE_SPECIFICATION);

        $specification->check($response);
    }

    public function testSpecificationTest7()
    {
        $this->createContainer();

        $response = [
            'field' => [
                'string' => null
            ]
        ];

        $specification = SpecificationFactory::make(ObjectType::class, [
            [
                'field' => new ObjectType([
                    'string' => new StringType(true)
                ])
            ]
        ])->setSpecificationType(AType::RESPONSE_SPECIFICATION);

        $specification->check($response);

        $this->assertTrue($specification->check($response));
    }

    public function testSpecificationTest8()
    {
        $this->setExpectedException(RuntimeException::class, 'Nullable types are not allowed for REQUEST specifications.');

        $this->createContainer();

        $response = [
            'field' => [
                'string' => null
            ]
        ];

        $specification = SpecificationFactory::make(ObjectType::class, [
            [
                'field' => new ObjectType([
                    'string' => new StringType(true, true)
                ])
            ]
        ])->setSpecificationType(AType::REQUEST_SPECIFICATION);

        $specification->check($response);

        $this->assertTrue($specification->check($response));
    }

    public function testSpecificationTest9()
    {
        $this->createContainer();

        $response = [1, 2, 3, 4, null];

        $specification = SpecificationFactory::make(CollectionType::class, [
            function () {
                return new IntegerType();
            }
        ])->setSpecificationType(AType::REQUEST_SPECIFICATION);

        $specification->check($response);

        $this->assertEquals($specification->getErrorMessages(), [
            4 => [
                "Value can not be null"
            ]
        ]);
    }


    public function testSpecificationTest10()
    {
        $this->createContainer();

        $response = [1, 2, 3, 4, null];

        $specification = SpecificationFactory::make(CollectionType::class, [
            function () {
                return new IntegerType();
            }
        ])->setSpecificationType(AType::RESPONSE_SPECIFICATION);

        $specification->check($response);

        $this->assertEquals($specification->getErrorMessages(), [
            4 => [
                "Value can not be null"
            ]
        ]);
    }


    public function testSpecificationTest11()
    {
        $this->createContainer();

        $response = [1, 2, 3, 4, null];

        $specification = SpecificationFactory::make(CollectionType::class, [
            function () {
                return new IntegerType(true);
            }
        ])->setSpecificationType(AType::RESPONSE_SPECIFICATION);

        $this->assertTrue($specification->check($response));
    }

    public function testResponsePaginationSpecificationTest()
    {
        $this->createContainer();

        $response = [
            'items' => [
                [
                    'field' => [
                        'string' => $this->faker->sentence(),
                        'enum' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                        'string2' => $this->faker->sentence(),
                        'array1' => [['a' => $this->faker->sentence(), 'b' => $this->faker->sentence()]],
                        'array2' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                        'array3' => [$this->faker->randomFloat(), $this->faker->randomFloat(), $this->faker->randomFloat()],
                        'emptyArray' => [],
                        'emptyArray2' => [$this->faker->randomFloat()],
                        'bool' => true,
                        'int' => $this->faker->randomNumber(),
                        'phone' => '131232131'
                    ],
                ],
                [
                    'field' => [
                        'string' => $this->faker->sentence(),
                        'enum' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                        'string2' => $this->faker->sentence(),
                        'array1' => [['a' => $this->faker->sentence(), 'b' => $this->faker->sentence()]],
                        'array2' => [$this->faker->sentence(), $this->faker->sentence(), $this->faker->sentence()],
                        'array3' => [$this->faker->randomFloat(), $this->faker->randomFloat(), $this->faker->randomFloat()],
                        'emptyArray' => [],
                        'emptyArray2' => [$this->faker->randomFloat()],
                        'bool' => true,
                        'int' => $this->faker->randomNumber(),
                        'phone' => '131232131'
                    ],
                ]
            ],
            'pagination' => [
                'totalEntries' => 1,
                'entriesOnCurrentPage' => 1,
                'entriesPerPageRequested' => 2,
                'currentPage' => 3,
                'totalPages' => 2
            ]
        ];

        $specification = PaginationSpecificationFactory::make(SpecificationMock::class);

        if (!$specification->check($response)) {
            dd($specification->getErrorMessages());
        }

        $this->assertTrue($specification->check($response));
    }

    public function testEntitySpecificationExtract()
    {
        $this->createContainer();

        $entity = new EntityMock();

        $entity->setId("1")
            ->setDescription("12345678")
            ->setName("vasia")
            ->setEntity((new SubEntityMock())
                ->setName('1111')
                ->setDescription('12232131')
                ->setId('222222')
            )
            ->setCollection(new Collection([1, 2, 3, 4, 5, 6, 7]));

        $specification = SpecificationFactory::make(EntitySpecificationMock::class);

        $result = $specification->extract($entity, [
            'resolvers' => [
                'id' => function ($entity) {
                    return $entity->getEntity()->getId();
                },
                'entity.id' => function ($entity) {
                    return 2222222211112;
                },
                'collection.*' => function ($entity) {
                    return 2;
                }
            ]
        ]);

        $this->assertEquals($result, [
            "id" => 222222,
            "name" => "vasia",
            "description" => "12345678",
            "entity" => [
                "id" => 2222222211112,
                "name" => "1111",
                "description" => "12232131",
            ],
            "collection" => [2, 2, 2, 2, 2, 2, 2]
        ]);
    }


    public function testEntitySpecificationCollectionRootExtract()
    {
        $this->createContainer();

        $entity = new EntityMock();

        $entity->setId("1")
            ->setDescription("12345678")
            ->setName("vasia")
            ->setEntity((new SubEntityMock())
                ->setName('1111')
                ->setDescription('12232131')
                ->setId('222222')
            )
            ->setCollection(new Collection([1, 2, 3, 4, 5, 6, 7]));

        $testCollection = new Collection([$entity]);

        $specification = SpecificationFactory::make(CollectionType::class, [function () {
            return new IntegerType();
        }]);

        $result = $specification->extract($testCollection, [
            'resolvers' => [
                '*' => function ($entity) {
                    return $entity->getEntity()->getId();
                }
            ]
        ]);

        $this->assertEquals($result, ["222222"]);
    }
} 