<?php
namespace App\Tests\QA\Endpoints\Specification;

use App\Libraries\Specification\Types\Collection\CollectionType;
use App\Libraries\Specification\Types\Custom\PhoneType;
use App\Libraries\Specification\Types\Object\ObjectType;
use App\Libraries\Specification\Types\Scalar\BooleanType;
use App\Libraries\Specification\Types\Scalar\FloatType;
use App\Libraries\Specification\Types\Scalar\IntegerType;
use App\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SpecificationMock extends ObjectType
{

    protected $specificationType = self::REQUEST_SPECIFICATION;

    /**
     * @return array
     */
    public function structure()
    {
        return [
            'field' => new ObjectType([
                'string' => new StringType(),
                'enum' => new CollectionType(function () {
                    return new StringType();
                }),
                'string2' => new StringType(),
                'array1' => new CollectionType(function () {
                    return new SingleLevelSpecificationMock();
                }, false, false),
                'array2' => new CollectionType(function () {
                    return new StringType();
                }, false, false),
                'array3' => new CollectionType(function () {
                    return new FloatType();
                }),
                'emptyArray' => new CollectionType(function () {
                    return new FloatType();
                }),
                'emptyArray2' => new CollectionType(function () {
                    return new FloatType();
                }),
                'bool' => new BooleanType(),
                'int' => new IntegerType(),
                'phone' => new PhoneType()
            ])
        ];
    }
}