<?php
namespace App\Tests\QA\Endpoints\Entity;

use App\QA\Endpoints\Entity\AbstractEntity;
use Mockery;
use App\QA\Endpoints\Faker\Populator;

/**
 * @author Sergei Melnikov <me@rnr.name>
 * @method entity($testValue, $times)
 * @method callbackTest($testValue)
 */
class Entity extends AbstractEntity
{
    /**
     * @return array
     */
    protected function generators()
    {
        return [
            'entity' => EntityGenerator::class
        ];
    }

    /**
     * @return Populator
     */
    protected function createPopulator()
    {
        return Mockery::mock(Populator::class);
    }
}