<?php
namespace App\Tests\QA\Endpoints\Entity;

use PHPUnit_Framework_TestCase;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class AbstractEntityTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testSimple()
    {
        $entity = new Entity();
        $this->assertEquals('test-test-test-', $entity->entity('test-', 3));
    }
}