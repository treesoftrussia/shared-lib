<?php
namespace App\Tests\QA\Endpoints\Entity;

use App\QA\Endpoints\Entity\AbstractGenerator;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class EntityGenerator extends AbstractGenerator
{
    /**
     * @param string $testValue
     * @param int $times
     * @return string
     */
    public function generate($testValue, $times)
    {
        return str_repeat($testValue, $times);
    }
}