<?php
namespace App\Tests\Libraries\Cast;

use App\Libraries\Cast\Cast;
use App\Libraries\Cast\CastException;
use PHPUnit_Framework_TestCase;
use ArrayObject;
use ArrayIterator;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class CastTest extends PHPUnit_Framework_TestCase
{
    public function testPositiveNumber()
    {
        $this->assertEquals(102, Cast::int('102'));
        $this->assertEquals(102, Cast::int(102));

        $this->assertEquals(102.98123, Cast::float('102.98123'));
        $this->assertEquals(102.98123, Cast::float(102.98123));
    }

    public function testNegativeNumber()
    {
        $this->assertEquals(-102, Cast::int('-102'));
        $this->assertEquals(-102, Cast::int(-102));

        $this->assertEquals(-102.98123, Cast::float('-102.98123'));
        $this->assertEquals(-102.98123, Cast::float(-102.98123));
    }

    public function testSetIntToFloat()
    {
        $this->assertEquals(102, Cast::float(102));
    }

    public function testSetFloatToInt()
    {
        $this->setExpectedException(CastException::class, 'Cannot convert float to int');

        $this->assertEquals(102, Cast::int(102.98123));
    }

    public function testValueProvider()
    {
        $value = new ValueProvider();
        $value->setValue(new ArrayObject());

        $this->assertInstanceOf(ArrayObject::class, Cast::object($value, ArrayObject::class)->getValue());

        $value = new ValueProvider();

        $this->assertNull(Cast::bool($value)->getValue());
    }

    public function testObject()
    {
        $this->setExpectedException(CastException::class, 'The value must be instance of "ArrayObject"');

        Cast::object(new ArrayIterator(), ArrayObject::class);
    }
} 