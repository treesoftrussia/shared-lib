<?php
namespace App\Tests\Libraries\Adapter;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class SanitizersProvider
{
    public function add20($value)
    {
        return $value + 20;
    }
} 