<?php
namespace App\Tests\Libraries\Adapter\Mapper;

use App\Libraries\Enum\Enum;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestCacheEnum extends Enum
{
    const VALUE_1 = 1;
    const VALUE_2 = 2;
    const VALUE_3 = 3;
} 