<?php
namespace App\Tests\Libraries\Validator;

use App\Libraries\Enum\Enum;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class EnumMock extends Enum
{
    const VALUE_1 = 'value1';
    const VALUE_2 = 'value2';
} 