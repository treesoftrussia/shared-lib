<?php
namespace App\Tests\Libraries\Validator;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ValidatorsTest extends \PHPUnit_Framework_TestCase
{
    public function testEnum()
    {
        $factory = new ValidatorFactoryMock();

        $validator = $factory->make([
            'good' => 'value1',
            'bad' => 'oops'
        ], [
            'good' => 'enum:'.EnumMock::class,
            'bad' => 'enum:'.EnumMock::class
        ]);

        $this->assertTrue($validator->fails());
        $this->assertArrayHasKey('bad', $validator->errors()->toArray());
    }
} 