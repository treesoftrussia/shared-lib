<?php
namespace App\Tests\Libraries\Validator;

use Illuminate\Validation\Factory;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ValidatorFactoryMock extends Factory
{
    public function __construct()
    {

    }

    public function make(array $data, array $rules, array $messages = array(), array $customAttributes = array())
    {
        return new ValidatorsMock(new TranslatorMock(), $data, $rules, $messages, $customAttributes);
    }
} 