<?php
namespace App\Tests\Libraries\Validator;
use App\Libraries\Validator\SharedValidators;
use App\Libraries\Validator\StateExistenceInterface;
use App\Libraries\Validator\UserExistenceInterface;
use Illuminate\Log\Writer;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ValidatorsMock extends SharedValidators
{
    /**
     * @return UserExistenceInterface
     */
    protected function getUserExistence()
    {
        // TODO: Implement getUserExistence() method.
    }

    /**
     * @return StateExistenceInterface
     */
    protected function getStateExistence()
    {
        // TODO: Implement getStateExistence() method.
    }

    /**
     * @return Writer
     */
    protected function getLogger()
    {
        // TODO: Implement getLogger() method.
    }
}