<?php
namespace App\Tests\Libraries\Processor;

use App\QA\Support\JsonRequest;
use Faker\Factory;
use App\Tests\Libraries\Mocks\CompanyMock;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\Container as ContainerInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Illuminate\Validation\Factory as ValidatorFactory;


/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class BaseProcessorTest extends \PHPUnit_Framework_TestCase
{
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function testGet()
    {
        $request = new JsonRequest(Request::METHOD_POST, [
            'key1' => 10,
            'key2' => 2,
            'key3' => 'test',
            'none' => '',
            'key4' => 'test',
            'key5' => 'test_is_test'
        ]);

        $processor = new TestProcessor($this->createContainer($request));
        TestProcessor::setSharedModifiersProvider(new SharedModifiersProvider());

        $this->assertEquals(10, $processor->get('key1'));
        $this->assertEquals(30, $processor->get(['none', 'key1', 'key2'], null, 'multiple'));
        $this->assertEquals('', $processor->get(['none', 'none1', 'none3']));
        $this->assertEquals(6, $processor->get('key2', null, 'multiple'));
        $this->assertEquals('_test_', $processor->get('key3', null, 'wrap'));
        $this->assertEquals('_33_', $processor->get('none', 11, 'multiple|wrap'));

        $this->assertEquals('test', $processor->get('key4', 'invalid'));
        $this->assertEquals('invalid', $processor->get('key4', 'invalid', null, true));
        $this->assertEquals('test_is_test', $processor->get('key5', '', null, true));

        $request = new JsonRequest(Request::METHOD_DELETE, [
            'key1' => 10
        ]);

        $processor = new TestProcessor($this->createContainer($request));

        $this->assertEquals(10, $processor->get('key1'));
    }

    public function testValidateOptions()
    {
        $request = new JsonRequest(Request::METHOD_POST, [
            'key1' => 'aaa',
            'key2' => 'a'
        ], [
            'option1' => 'q',
            'option2' => 'qqq'
        ]);

        $processor = new TestOptionsValidatorProcessor($this->createContainer($request));
        $options = new TestOptionsProcessor($this->createContainer($request));

        $errors = $processor->validate();

        $this->assertEquals('key2', key($errors));

        $errors = $options->validate();

        $this->assertEquals('option1', key($errors));
    }

    public function testFlatten()
    {
        $request = new JsonRequest(Request::METHOD_POST, [
            'data' => [10, 42, 10]
        ]);

        $processor = new TestProcessor($this->createContainer($request));

        $this->assertTrue(array_equal($processor->get('data'), [10, 42, 10]));
    }


    public function testValidateBeforeAndAfter()
    {
        $request = new JsonRequest(Request::METHOD_POST, [
            'key1' => 'test_is_test_as_test',
            'key2' => 'test'
        ]);

        $processor = new ValidatorTestProcessor($this->createContainer($request));

        $errors = $processor->validate();

        $this->assertTrue(isset($errors['key1']));
        $this->assertFalse(isset($errors['key2']));
        $this->assertTrue(isset($errors['key3']));
    }

    public function testValidateAllowable()
    {
        $all = [
            'name' => $this->faker->company,
            'pending' => true,
            'contact' => [
                'firstName' => $this->faker->firstName,
                'lastName' => $this->faker->lastName
            ]
        ];

        $processor = new RequestValidatorProcessor($this->createContainer(new JsonRequest(Request::METHOD_POST, $all)));

        /**
         * @var CompanyMock $object
         */
        $object = $processor->populate(new CompanyMock());

        $this->assertInstanceOf(CompanyMock::class, $object);
        $this->assertEquals($all['name'], $object->getName());
        $this->assertNull($object->getPending());
        $contact = $object->getContact();
        $this->assertNotNull($contact);
        $this->assertEquals($all['contact']['firstName'], $contact->getFirstName());
        $this->assertNull($contact->getLastName());
        $this->assertNull($contact->getType());
    }

    public function testValidateAllowableEmpty()
    {
        $this->setExpectedException(
            BadRequestHttpException::class,
            'There\'s no allowable data in the body.');

        $processor = new RequestValidatorProcessor($this->createContainer(new Request()));

        $processor->populate(new CompanyMock());
    }

    public function testOptions()
    {
        $request = new JsonRequest(Request::METHOD_POST, [], [
            'keepInQueue' => true,
            'notify' => [
                'client' => true,
                'appraiser' => false,
                'none' => true
            ]
        ]);

        $processor = new TestOptionsProcessor($this->createContainer($request));

        /**
         * @var TestOptionsHolder $options
         */
        $options = $processor->createOptions();

        $this->assertTrue($options->getKeepInQueue());
        $this->assertTrue($options->getNotify()->getClient());
        $this->assertFalse($options->getNotify()->getAppraiser());
        $this->assertNull($options->getNotify()->getNone());
    }

    public function testPopulateFilter()
    {
        $request = new JsonRequest('get', [
            'filter' => [
                'string' => 'test',
                'number' => '10',
                'date' => '2001-09-10 12:09:01',
                'enum' => '10',
                'object' => [
                    'value' => 'works'
                ]
            ]
        ]);

        $processor = new FilterProcessor($this->createContainer($request));

        $filter = $processor->createFilter();

        $this->assertNull($filter->getString());
        $this->assertNull($filter->getNumber());

        $this->assertTrue($filter->getEnum()->is(FilterEnum::VALUE));
        $this->assertEquals('2001-09-10 12:09:01', $filter->getDate()->format('Y-m-d H:i:s'));
        $this->assertEquals('works', $filter->getObject()->getValue());
    }

    private function createContainer(Request $request)
    {
        $container = new Container();
        $container->instance(ValidatorFactory::class, new ValidatorFactoryMock());
        $container->instance(Request::class, $request);
        $container->instance(ContainerInterface::class, $container);

        return $container;
    }
} 