<?php
namespace App\Tests\Libraries\Processor;

use App\Libraries\Enum\Enum;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class FilterEnum extends Enum
{
    const VALUE = 10;
} 