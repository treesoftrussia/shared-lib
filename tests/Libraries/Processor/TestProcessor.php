<?php
namespace App\Tests\Libraries\Processor;

use App\Libraries\Modifier\Manager;
use App\Libraries\Processor\AbstractProcessor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestProcessor extends AbstractProcessor
{
    protected function rules()
    {
        return [
            'key4' => 'max:10|min:5',
            'key5' => 'min:10',
        ];
    }

    protected function modifiers(Manager $manager)
    {
        $manager->register('multiple', function ($value) {
            return $value * 3;
        });
    }
}