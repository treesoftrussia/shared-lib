<?php
namespace App\Tests\Libraries\Processor;

use App\Libraries\Validator\PostValidationInterface;
use App\Libraries\Validator\PostValidationTrait;
use Illuminate\Validation\Validator;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ValidatorMock extends Validator implements PostValidationInterface
{
    use PostValidationTrait;
} 