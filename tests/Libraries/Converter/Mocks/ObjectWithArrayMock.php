<?php
namespace App\Tests\Libraries\Converter\Mocks;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ObjectWithArrayMock
{
    private $contacts;

    public function getContacts()
    {
        return $this->contacts;
    }

    public function setContacts(array $contacts)
    {
        $this->contacts = $contacts;

        return $this;
    }
}