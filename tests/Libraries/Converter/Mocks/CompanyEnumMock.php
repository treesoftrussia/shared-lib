<?php
namespace App\Tests\Libraries\Converter\Mocks;

use App\Tests\Libraries\Mocks\CompanyMock;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class CompanyEnumMock extends CompanyMock
{
    private $enumerableField;

    public function getEnumerableField()
    {
        return $this->enumerableField;
    }

    public function setEnumerableField(EnumMock $enumerableField)
    {
        $this->enumerableField = $enumerableField;

        return $this;
    }
}