<?php
namespace App\Tests\Libraries\Mocks\Enum;

use App\Libraries\Enum\Enum;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class Test2Enum extends Enum
{
    const TEST_FIELD = 'test-field';
}