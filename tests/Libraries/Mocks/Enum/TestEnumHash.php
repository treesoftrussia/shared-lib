<?php
namespace App\Tests\Libraries\Mocks\Enum;
use App\Libraries\Enum\EnumHash;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class TestEnumHash extends EnumHash
{
    /**
     * @return string
     */
    public function getEnumClass()
    {
        return TestEnum::class;
    }
}