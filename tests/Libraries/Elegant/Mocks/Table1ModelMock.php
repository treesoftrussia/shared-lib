<?php
namespace App\Tests\Libraries\Elegant\Mocks;

use App\Libraries\Elegant\Elegant;

/**
 * @author Sergei Melnikov <me@rnr.name>
 * @property mixed $attribute1
 * @property mixed $attribute2
 * @property mixed $attribute3
 */
class Table1ModelMock extends Elegant
{
    /**
     * @return array
     */
    public function getColumns()
    {
        return ['attribute1', 'attribute2', 'attribute3'];
    }
}