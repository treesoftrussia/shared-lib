<?php
namespace App\Tests\Libraries\Elegant\Mocks;

use App\Libraries\Elegant\Sorting\AbstractSortSpecification;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class NestedSortSpecification extends AbstractSortSpecification
{
    /**
     * @return string|string[]
     */
    protected function getSupportedModelClasses()
    {
        return ModelMock::class;
    }

    /**
     * @return array
     */
    public function specification()
    {
        return [
            'field' => 'nested-string',
            'callable' => function ($query, $direction, $key) {
                /**
                 * @var QueryMock $query
                 */
                $query->orderBy('nested-callable', $direction);
            }
        ];
    }
}