<?php
namespace App\Tests\Libraries\Elegant\Mocks;
use App\Libraries\Elegant\AbstractModifier;
use string;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class TestModifier extends AbstractModifier
{
    /**
     * @param QueryMock $query
     * @param $value
     */
    public function applyKeyword(QueryMock $query, $value) {
        $query->addTrace(__METHOD__, $value);
    }

    /**
     * @param QueryMock $query
     * @param $value
     */
    public function applyIds(QueryMock $query, $value) {
        $query->addTrace(__METHOD__, $value);
    }

    /**
     * @param QueryMock $query
     * @param $property
     */
    public function applyProperty(QueryMock $query, TestPropertyFilter $property)
    {
        $query->modify(new PropertyModifier($property));
    }

    /**
     * @return string|string[]
     */
    protected function getSupportedModelClasses()
    {
        return ModelMock::class;
    }
}