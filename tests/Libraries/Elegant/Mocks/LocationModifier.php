<?php
namespace App\Tests\Libraries\Elegant\Mocks;
use App\Libraries\Elegant\AbstractModifier;
use string;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class LocationModifier extends AbstractModifier
{
    /**
     * @param QueryMock $query
     * @param $value
     */
    public function applyAddress(QueryMock $query, $value)
    {
        $query->addTrace(__METHOD__, $value);
    }

    /**
     * @param QueryMock $query
     * @param $value
     */
    public function applyCountry(QueryMock $query, $value)
    {
        $query->addTrace(__METHOD__, $value);
    }

    /**
     * @return string|string[]
     */
    protected function getSupportedModelClasses()
    {
        return ModelMock::class;
    }
}