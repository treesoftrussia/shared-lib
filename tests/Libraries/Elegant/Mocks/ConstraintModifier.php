<?php
namespace App\Tests\Libraries\Elegant\Mocks;
use App\Libraries\Elegant\AbstractModifier;
use string;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ConstraintModifier extends AbstractModifier
{
    /**
     * @return string|string[]
     */
    protected function getSupportedModelClasses()
    {
        return [
            ModelMock2::class,
            ModelMock::class
        ];
    }
}