<?php
namespace App\Libraries\Cast\Casters;
use App\Libraries\Cast\CastException;
use App\Libraries\Cast\Support;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class FloatCaster implements CasterInterface
{
    /**
     * @param mixed $value
     * @return float
     * @throws CastException
     */
    public function cast($value)
    {
        if (is_float($value) || is_int($value)) {
            return (float)$value;
        }

        if ($value === '') {
            return null;
        }

        if (substr_count($value, '.') > 1) {
            throw new CastException('Cannot convert value to float');
        }

        list($minus, $value) = Support::getNumberParts($value);

        $i = str_replace('.', '', $value);

        if (!ctype_digit($i)) {
            throw new CastException('Cannot convert non-numeric type to float');
        }

        return (float)($minus.$value);
    }
} 