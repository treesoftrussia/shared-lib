<?php
namespace App\Libraries\Cast\Casters;
use App\Libraries\Cast\CastException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface CasterInterface
{
    /**
     * @param mixed $value
     * @return mixed
     * @throws CastException
     */
    public function cast($value);
} 