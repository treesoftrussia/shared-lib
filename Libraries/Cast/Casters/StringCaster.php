<?php
namespace App\Libraries\Cast\Casters;
use App\Libraries\Cast\CastException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class StringCaster implements CasterInterface
{
    /**
     * @param mixed $value
     * @return string
     * @throws CastException
     */
    public function cast($value)
    {
        if (is_string($value)) {
            return $value;
        }

        if (!is_scalar($value)) {
            throw new CastException('Only scalar types can be converted to string');
        }

        return (string)$value;
    }
} 