<?php
namespace App\Libraries\Cast\Casters;
use App\Libraries\Cast\CastException;
use App\Libraries\Cast\Support;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class IntCaster implements CasterInterface
{
    /**
     * @param mixed $value
     * @return int
     * @throws CastException
     */
    public function cast($value)
    {
        if (is_int($value)) {
            return $value;
        }

        if ($value === '') {
            return null;
        }

        if (substr_count($value, '.') === 1){
            throw new CastException('Cannot convert float to int');
        }

        list($minus, $value) = Support::getNumberParts($value);

        if (!ctype_digit($value)) {
            throw new CastException('Cannot convert non-numeric type to int');
        }

        return (int) ($minus.$value);
    }
} 