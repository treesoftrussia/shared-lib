<?php
namespace App\Libraries\Cast\Casters;
use App\Libraries\Cast\CastException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class BoolCaster implements CasterInterface
{
    /**
     * @param mixed $value
     * @return bool
     * @throws CastException
     */
    public function cast($value)
    {
        if (is_bool($value)) {
            return $value;
        }

        if ($value === ''){
            return null;
        }

        throw new CastException('Value must be boolean');
    }
} 