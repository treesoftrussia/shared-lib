<?php
namespace App\Libraries\Cast;

use App\Libraries\Cast\Casters\BoolCaster;
use App\Libraries\Cast\Casters\CasterInterface;
use App\Libraries\Cast\Casters\CollectionCaster;
use App\Libraries\Cast\Casters\FloatCaster;
use App\Libraries\Cast\Casters\IntCaster;
use App\Libraries\Cast\Casters\ObjectCaster;
use App\Libraries\Cast\Casters\StringCaster;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Cast
{
    /**
     * @param mixed $value
     * @return int|ValueProviderInterface
     * @throws CastException
     */
    public static function int($value)
    {
        return static::cast($value,  new IntCaster());
    }

    /**
     * @param mixed $value
     * @return float|ValueProviderInterface
     * @throws CastException
     */
    public static function float($value)
    {
        return static::cast($value,  new FloatCaster());
    }

    /**
     * @param mixed $value
     * @return bool|ValueProviderInterface
     * @throws CastException
     */
    public static function bool($value)
    {
        return static::cast($value,  new BoolCaster());
    }

    /**
     * @param mixed $value
     * @return string|ValueProviderInterface
     * @throws CastException
     */
    public static function string($value)
    {
        return static::cast($value,  new StringCaster());
    }

    /**
     * @param mixed $value
     * @return mixed[]|ValueProviderInterface
     * @throws CastException
     */
    public static function collection($value)
    {
        return static::cast($value,  new CollectionCaster());
    }

    /**
     * @param object $value
     * @param string $class
     * @return object|ValueProviderInterface
     * @throws CastException
     */
    public static function object($value, $class)
    {
        return static::cast($value, new ObjectCaster($class));
    }

    /**
     * @param mixed $value
     * @param CasterInterface $caster
     * @return mixed|ValueProviderInterface
     */
    private static function cast($value, CasterInterface $caster)
    {
        if (Support::isNull($value)){
            return $value;
        }

        if ($value instanceof ValueProviderInterface){
            $value->setValue($caster->cast($value->getValue()));
            return $value;
        }

        return $caster->cast($value);
    }
} 