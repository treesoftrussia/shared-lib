<?php namespace App\Libraries\Specification\Support;

use App\Libraries\Specification\Types\Collection\CollectionType;
use App\Libraries\Specification\Types\Object\ObjectType;
use App\Libraries\Specification\Types\Scalar\IntegerType;
use ReflectionClass;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SpecificationFactory
{
    public static function make($className, $args = [])
    {
        $specification = (new ReflectionClass($className))->newInstanceArgs($args);
        $specification->setContainer(app());
        return $specification;
    }
}