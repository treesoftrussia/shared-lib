<?php namespace App\Libraries\Specification\Support;

use App\Libraries\Specification\Types\Collection\CollectionType;
use App\Libraries\Specification\Types\Object\ObjectType;
use App\Libraries\Specification\Types\Scalar\IntegerType;
use ReflectionClass;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class PaginationSpecificationFactory extends SpecificationFactory
{
    public static function make($baseSpecification, $args = [])
    {
        return parent::make(ObjectType::class, [function () use ($baseSpecification, $args) {
            return [
                'items' => new CollectionType(function() use ($baseSpecification, $args) {
                    return (new ReflectionClass($baseSpecification))->newInstanceArgs($args);
                }, true, false),
                'pagination' => new ObjectType([
                        "totalEntries" => new IntegerType(),
                        "entriesOnCurrentPage" => new IntegerType(),
                        "entriesPerPageRequested" => new IntegerType(),
                        "currentPage" => new IntegerType(),
                        "totalPages" => new IntegerType()
                    ]
                )];
        }])->setSpecificationType(ObjectType::RESPONSE_SPECIFICATION);
    }
}