<?php namespace App\Libraries\Specification\Types\Scalar;
use App\Libraries\Cast\Cast;


/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class FloatType extends AScalarType
{
    protected function checkType($object){
        return is_float($object);
    }

    protected function failedCheckValueMessage()
    {
        return "Value is not a float.";
    }

    public function extract($entity)
    {
        return Cast::float($entity);
    }
}