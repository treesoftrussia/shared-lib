<?php namespace App\Libraries\Specification\Types\Scalar;
use App\Libraries\Cast\Cast;


/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class IntegerType extends AScalarType
{
    protected function checkType($object){
        return is_int($object);
    }

    protected function failedCheckValueMessage()
    {
        return "Value is not an integer.";
    }

    public function extract($entity)
    {
        return Cast::int($entity);
    }
}