<?php namespace App\Libraries\Specification\Types\Scalar;

use App\Libraries\Specification\ALeafType;
use App\Libraries\Specification\ValidationException;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
abstract class AScalarType extends ALeafType
{
    protected abstract function failedCheckValueMessage();

    protected abstract function checkType($object);

    protected function findErrors($object)
    {
        if (!is_scalar($object)) {
            throw new ValidationException('Value is not a scalar.');
        }

        if (!$this->checkType($object)) {
            throw new ValidationException($this->failedCheckValueMessage());
        }
    }
}