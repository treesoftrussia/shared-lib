<?php namespace App\Libraries\Specification\Types\Scalar;
use App\Libraries\Cast\Cast;


/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class BooleanType extends AScalarType
{
    protected function checkType($object){
        return is_bool($object);
    }

    protected function failedCheckValueMessage()
    {
        return "Value is not a boolean.";
    }

    public function extract($entity)
    {
        return Cast::bool($entity);
    }
}