<?php namespace App\Libraries\Specification\Types\Custom;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class PhoneType extends CustomType
{
    protected $type = self::STRING_TYPE;

    public function getRules()
    {
        return ['required', 'string', 'min:6', 'max:12'];
    }
}