<?php namespace App\Libraries\Specification\Types\Collection;

use App\Libraries\Kangaroo\Exceptions\RuntimeException;
use App\Libraries\Specification\ACompositeType;
use App\Libraries\Specification\AType;
use App\Libraries\Specification\Validatable;
use Illuminate\Support\Collection;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class CollectionType extends ACompositeType
{
    use Validatable;

    protected function findErrors($object)
    {
        $object = $this->handleArrayValue($object);

        /**
         * @var callable $newTypeCallback
         */
        $newTypeCallback = $this->structure();

        /**
         * @var AType $item
         */
        foreach ($object as $key => $itemToCheck){
            /**
             * @var AType $typeObject
             */
            $typeObject = $newTypeCallback();

            $this->bubbleEnvironment($typeObject);

            $this->getScope()->create($key);

            if(!$typeObject->check($itemToCheck)){
                $this->errors = array_merge($typeObject->getErrorMessages(), $this->errors);
            }
            $this->getScope()->drop();
        }
    }

    /**
     * @param $collection
     * @param array $options
     * @return array
     */
    public function extract($collection, $options = [])
    {

        if(is_null($collection) && $this->nullable){
            return null;
        }

        if(!$collection instanceof Collection){
            throw new RuntimeException("Entity ".get_class($collection)." is not a collection");
        }

        $this->handleOptions($options);

        $result = [];
        /**
         * @var callable $newTypeCallback
         */
        $newTypeCallback = $this->structure();

        foreach ($collection as $value){
            if($this->isResolvable('*')){
                $result[] = $this->resolve('*', $value);
            } else {
                /**
                 * @var AType $typeObject
                 */
                $typeObject = $newTypeCallback();
                $result[] = $typeObject->extract($value);
            }
        }
        return $result;
    }


}