<?php namespace App\Libraries\Specification\Types\Object;

use App\Libraries\Kangaroo\Exceptions\RuntimeException;
use App\Libraries\Specification\ACompositeType;
use App\Libraries\Specification\AType;
use App\Libraries\Specification\ValidationException;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ObjectType extends ACompositeType
{
    public function __construct($structure = null, $nullable = false, $optional = false)
    {
        parent::__construct($structure, false, $nullable, $optional);
    }

    private function checkExtraKeys($structure, $inputArray)
    {
        $diff = array_diff(array_keys($inputArray), array_keys($structure));
        if (!empty($diff)) {
            foreach ($diff as $diffItem) {
                if (is_integer($diffItem)) {
                    $this->errors[$this->getScope()->get($diffItem, true)][] = "Integer keys is not allowed (or no key detected)";
                } else {
                    $this->errors[$this->getScope()->get($diffItem, true)][] = "Key is not allowed";
                }
            }
        }
    }

    protected function findErrors($object)
    {
        $object = $this->handleArrayValue($object);

        $structure = is_callable($this->structure) ? call_user_func($this->structure()) : $this->structure();
        $this->checkExtraKeys($structure, $object);

        /**
         * @var AType $checkerItem
         */
        foreach ($structure as $key => $checkerItem) {
            $this->bubbleEnvironment($checkerItem);

            if (!array_key_exists($key, $object)) {
                if (!$checkerItem->isOptional()) {
                    throw new ValidationException('Value is required');
                }

                if($checkerItem->isOptional() && $checkerItem->getSpecificationType() == self::RESPONSE_SPECIFICATION){
                    throw new RuntimeException('Optional types are not allowed for RESPONSE specifications.');
                }

                continue;
            }

            $this->getScope()->create($key);

            if (!$checkerItem->check($object[$key])) {
                $this->errors = array_merge($checkerItem->getErrorMessages(), $this->errors);
            }

            $this->getScope()->drop();
        }
    }


    protected function tryToGetFromGetter($structureKey, $entity)
    {
        $getter = make_getter($structureKey);

        if (method_exists($entity, $getter)) {
            return $entity->$getter();
        } else {
            throw new RuntimeException("Method " . $getter . " doesn't exists in object " . get_class($entity));
        }
    }

    public function extract($entity, $options = [])
    {
        if(is_null($entity) && $this->nullable){
            return null;
        }

        $this->handleOptions($options);

        $result = [];
        $structure = $this->structure();

        foreach ($structure as $structureKey => $structureItem) {

            if ($this->isResolvable($structureKey, $entity)) {
                $extractedEntity = $this->resolve($structureKey, $entity);
            } else {
                $extractedEntity = $this->tryToGetFromGetter($structureKey, $entity);
            }

            $this->bubbleResolvers($structureKey, $structureItem);

            $result[$structureKey] = $structureItem->extract($extractedEntity);
        }
        return $result;
    }
//
//    public function fake($options = []){
//        $result = [];
//        $structure = $this->structure();
//
//        foreach ($structure as $structureKey => $structureItem){
//
//            $result[$structureKey] = $structureItem->extract($extractedEntity);
//        }
//        return $result;
//    }
}