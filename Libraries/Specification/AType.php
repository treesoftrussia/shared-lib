<?php namespace App\Libraries\Specification;

use App\Libraries\Converter\Populator\Populator;
use App\Libraries\Converter\Support\Scope;
use App\Libraries\Kangaroo\Exceptions\RuntimeException;
use Illuminate\Contracts\Container\Container;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
abstract class AType
{
    const REQUEST_SPECIFICATION = 1;
    const RESPONSE_SPECIFICATION = 2;

    protected $errors = [];
    public $container;
    protected $specificationType = self::RESPONSE_SPECIFICATION;

    protected $nullable;
    protected $scope;
    protected $optional;

    public function __construct($nullable = false, $optional = false)
    {
        $this->nullable = $nullable;
        $this->optional = $optional;
    }

    protected function getScope()
    {
        if (!$this->scope) {
            $this->scope = new Scope();
        }
        return $this->scope;
    }

    protected function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }

    public function setContainer(Container $container)
    {
        $this->container = $container;
        return $this;
    }

    public function setSpecificationType($specificationType)
    {
        $this->specificationType = $specificationType;
        return $this;
    }

    public function getSpecificationType()
    {
        return $this->specificationType;
    }

    protected function isOptional(){
        return $this->optional;
    }

    private function skipIfNullable($object)
    {
        if($this->nullable && $this->specificationType == self::REQUEST_SPECIFICATION){
            throw new RuntimeException('Nullable types are not allowed for REQUEST specifications.');
        }

        if(is_null($object) && !$this->nullable){
            throw new ValidationException('Value can not be null');
        } else if(is_null($object)){
            return true;
        }

        return false;
    }

    public function check($object)
    {
        if($this->skipIfNullable($object)){
            return true;
        }

        try {
            $this->findErrors($object);
        } catch (ValidationException $e) {
            $currentScope = $this->getScope()->get(null, true);
            if(empty($currentScope)){
                $this->errors = $e->getErrors();
            } else {
                $this->errors[$this->getScope()->get(null, true)] = $e->getErrors();
            }
        };

        return empty($this->errors);
    }

    protected abstract function findErrors($object);

    public abstract function extract($entity);

    public function getErrorMessages()
    {
        return $this->errors;
    }
}