<?php
namespace App\Libraries\Specification;


use App\Libraries\Kangaroo\Exceptions\RuntimeException;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ValidationException extends RuntimeException
{
    private $errors;
    private $key;
    private $secondLevel;

    public function __construct($errors, $key = null, $secondLevel = false)
    {
        if(is_array($errors)){
            $this->errors = $errors;
        } else {
            $this->errors[] = $errors;
        }
        $this->secondLevel = $secondLevel;
        $this->key = $key;
    }

    public function getErrors(){
        return $this->errors;
    }

    public function isSecondLevel()
    {
        return $this->secondLevel;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }
}