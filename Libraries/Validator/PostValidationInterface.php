<?php
namespace App\Libraries\Validator;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface PostValidationInterface
{
    public function setAfterRules(array $rules);
} 