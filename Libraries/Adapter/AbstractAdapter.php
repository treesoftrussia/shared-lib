<?php
namespace App\Libraries\Adapter;

use App\Libraries\Modifier\Manager;
use App\Libraries\Modifier\ModifierProviderTrait;
use Illuminate\Support\Collection;
use App\Libraries\Elegant\Collection as ElegantCollection;

/**
 * A base class for all adapters
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractAdapter
{
    use ModifierProviderTrait;

    const M_SANITIZER = 'sanitizer';
    const M_MODIFIER = 'modifier';

    /**
     * The method provides an ModifierManager setup to use for transformation
     *
     * @return Manager
     */
    protected function modifier()
    {
        return $this->getModifierManager(static::M_MODIFIER);
    }

    /**
     * The method provides an Manager setup to use for extraction
     *
     * @return Manager
     */
    protected function sanitizer()
    {
        return $this->getModifierManager(static::M_SANITIZER);
    }
}