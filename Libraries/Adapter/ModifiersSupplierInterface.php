<?php
namespace App\Libraries\Adapter;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface ModifiersSupplierInterface
{
    /**
     * @param object $provider
     */
    public function setSanitizersProvider($provider);

    /**
     * @param object $provider
     */
    public function setModifiersProvider($provider);
} 