<?php
namespace App\Libraries\Adapter;
use App\Libraries\Elegant\Elegant;
use App\Libraries\Elegant\Collection;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
trait TransformCollectionTrait
{
    /**
     * @param Elegant $model
     * @return mixed
     */
    abstract public function transform(Elegant $model);

    /**
     * Creates and populates the collection of entities with data from the collection of models
     *
     * @param Collection $collection
     * @return array
     */
    public function transformCollection(Collection $collection)
    {
        $data = [];

        foreach ($collection as $model) {
            $data[] = $this->transform($model);
        }

        return $data;
    }

}