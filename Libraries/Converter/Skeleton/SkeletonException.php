<?php
namespace App\Libraries\Converter\Skeleton;

use Exception;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class SkeletonException extends Exception
{
}
