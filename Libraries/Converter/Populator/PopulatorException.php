<?php
namespace App\Libraries\Converter\Populator;

use Exception;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class PopulatorException extends Exception
{
}