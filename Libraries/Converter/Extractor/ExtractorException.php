<?php
namespace App\Libraries\Converter\Extractor;

use Exception;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ExtractorException extends Exception
{
}