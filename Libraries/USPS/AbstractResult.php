<?php
namespace App\Libraries\USPS;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractResult
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
} 