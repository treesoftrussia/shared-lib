<?php
namespace App\Libraries\USPS;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class CityStateLookupResult extends AbstractResult
{
    public function getState()
    {
        return $this->data['ZipCode']['State'];
    }

    public function getZip()
    {
        return $this->data['ZipCode']['Zip5'];
    }

    public function getCity()
    {
        return $this->data['ZipCode']['City'];
    }
} 