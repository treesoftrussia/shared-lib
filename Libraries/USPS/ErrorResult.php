<?php
namespace App\Libraries\USPS;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ErrorResult extends AbstractResult
{
    public function getMessage()
    {
        return $this->data['ZipCode']['Error']['Description'];
    }
} 