<?php
namespace App\Libraries\USPS;

/**
 * Abstract class for all USPS commands. It provides some base methods for all commands
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractCommand
{
    /**
     * Executes the command
     * @return array
     */
    abstract function execute();

    /**
     * Sends request based on provided url and post data
     * @param string $url
     * @param array $data
     * @return array
     * @throws RequestException
     */
    protected function send($url, array $data)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);

        if ($result === false) {
            $errorMessage = curl_error($ch);
            $errorCode = curl_errno($ch);
            curl_close($ch);
            throw new RequestException($errorMessage, $errorCode);
        }

        curl_close($ch);

        /*
         * This is a tricky way to convert xml string to array
         */
        return json_decode(json_encode(new \SimpleXMLElement($result)), true);
    }
} 