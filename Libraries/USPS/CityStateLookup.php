<?php
namespace App\Libraries\USPS;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class CityStateLookup extends AbstractCommand
{
    private $zip;

    public function __construct($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Executes requests
     * @return CityStateLookupResult|ErrorResult
     */
    public function execute()
    {
        $userId = \Config::get('services.usps.user_id');
        $url = \Config::get('services.usps.api_url');

        $xml = '<CityStateLookupRequest USERID="' . $userId . '">';
        $xml .= '<ZipCode>';
        $xml .= '<Zip5>' . $this->zip . '</Zip5>';
        $xml .= '</ZipCode>';
        $xml .= '</CityStateLookupRequest>';

        $data = $this->send($url, [
            'API' => 'CityStateLookup',
            'xml' => $xml
        ]);

        if (isset($data['ZipCode']['Error'])) {
            return new ErrorResult($data);
        }

        return new CityStateLookupResult($data);
    }
} 