<?php namespace App\Libraries\Kangaroo;

class ErrorData
{

    private $httpCode;
    private $internalCode;
    private $data;


    public function __construct($httpCode, $internalCode, $data)
    {
        $this->httpCode = $httpCode;
        $this->internalCode = $internalCode;
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getHttpCode()
    {
        return $this->httpCode;
    }

    /**
     * @param mixed $httpCode
     * @return ErrorData
     */
    public function setHttpCode($httpCode)
    {
        $this->httpCode = $httpCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInternalCode()
    {
        return $this->internalCode;
    }

    /**
     * @param mixed $internalCode
     * @return ErrorData
     */
    public function setInternalCode($internalCode)
    {
        $this->internalCode = $internalCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

}