<?php namespace App\Libraries\Kangaroo;

use App\Http\ErrorCodes\ErrorCodes;


class ErrorDataFactory
{
    private static $config;

    public static function make(ErrorCodes $errorCode, $args){
        if(!self::$config){
            self::$config = config('errors');
        }

        $configValues = array_collapse(self::$config);
        
        foreach($configValues as $configValue){
            $currentHttpCode = $configValue['http_code'];

            foreach($configValue['errors'] as $error){
                if($errorCode->is($error['internal_code'])){
                    
                    if(isset($error['data']['message']['args'])){
                        $transArgs = self::replaceArgs($error['data']['message']['args'], $args);
                        $message = trans($error['data']['message']['trans'], $transArgs);
                    } else {
                        $message = is_array($error['data']['message']) && $error['data']['message']['trans']? trans($error['data']['message']['trans']) : $error['data']['message'];
                    }

                    if(count($error['data']) == 1){
                        return new ErrorData(
                            $currentHttpCode,
                            $error['internal_code'],
                            $message
                        );
                    } else {
                        $data = array();
                        $data['message'] = $message;
                        unset($error['data']['message']);

                        $data = array_merge($data, self::replaceArgs($error['data'], $args));

                        return new ErrorData(
                            $currentHttpCode,
                            $error['internal_code'],
                            $data
                        );
                    }
                }
            }
        }

        return null;
    }


    protected static function replaceArgs($source, $args){
        $data = [];
        foreach ($source as $key => $value){
            if(preg_match('/^\$([0-9]+)$/', $value, $matches)){
                $data[$key] = $args[(int)($matches[1])-1];
            }
        }
        return $data;
    }

}