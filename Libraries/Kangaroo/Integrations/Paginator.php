<?php
namespace App\Libraries\Kangaroo\Integrations;

use App\Libraries\Kangaroo\Pagination\PaginationInterface;
use League\Fractal\Pagination\PaginatorInterface;
use RuntimeException;

/**
 * The implementation of the Fractal's paginator interface in order to make the Transformer support pagination
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Paginator implements PaginatorInterface
{
    /**
     * @var PaginationInterface
     */
    private $pagination;

    public function __construct(PaginationInterface $pagination)
    {
        $this->pagination = $pagination;
    }

    /**
     * Get the current page.
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->pagination->getCurrent();
    }

    /**
     * Get the last page.
     *
     * @return int
     */
    public function getLastPage()
    {
        return $this->pagination->getTotalPages();
    }

    /**
     * Get the total.
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->pagination->getTotal();
    }

    /**
     * Get the number per page.
     *
     * @return int
     */
    public function getPerPage()
    {
        return $this->pagination->getPerPage();
    }

    /**
     * @return int
     * @throws RuntimeException
     */
    public function getCount()
    {
        return $this->pagination->getOnPage();
    }

    /**
     * @param int $page
     * @return string
     * @throws RuntimeException
     */
    public function getUrl($page)
    {
        return $this->pagination->generateUrl($page);
    }
}