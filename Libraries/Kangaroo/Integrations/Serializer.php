<?php
namespace App\Libraries\Kangaroo\Integrations;

use League\Fractal\Pagination\CursorInterface;
use League\Fractal\Pagination\PaginatorInterface;
use League\Fractal\Resource\ResourceInterface;
use League\Fractal\Serializer\SerializerAbstract;
use RuntimeException;

/**
 * The implementation of a standard SerializerAbstract class from Fractal
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Serializer extends SerializerAbstract
{
    /**
     * Serialize a collection.
     *
     * @param string $resourceKey
     * @param array $data
     *
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }

        return $data;
    }

    /**
     * Serialize an item.
     *
     * @param string $resourceKey
     * @param array $data
     *
     * @return array
     */
    public function item($resourceKey, array $data)
    {
        return $data;
    }

    /**
     * Serialize the included data.
     *
     * @param ResourceInterface $resource
     * @param array $data
     *
     * @return array
     */
    public function includedData(ResourceInterface $resource, array $data)
    {
        return $data;
    }

    /**
     * Serialize the meta.
     *
     * @param array $meta
     *
     * @return array
     */
    public function meta(array $meta)
    {
        if (!$meta) {
            return [];
        }

        return ['meta' => $meta];
    }

    /**
     * Serialize the paginator.
     *
     * @param PaginatorInterface $paginator
     *
     * @return array
     */
    public function paginator(PaginatorInterface $paginator)
    {
        return ['pagination' => [
            'total' => (int)$paginator->getTotal(),
            'onPage' => (int)$paginator->getCount(),
            'perPage' => (int)$paginator->getPerPage(),
            'current' => (int)$paginator->getCurrentPage(),
            'totalPages' => (int)$paginator->getLastPage(),
        ]];
    }

    public function cursor(CursorInterface $cursor)
    {
        throw new RuntimeException('Cursor is not supported by this serializer.');
    }
}