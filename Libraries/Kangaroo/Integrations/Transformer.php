<?php
namespace App\Libraries\Kangaroo\Integrations;

use App\Core\Support\Pagination\PaginationEntityInterface;
use App\Libraries\Kangaroo\Config;
use App\Libraries\Kangaroo\Pagination\PaginationProviderInterface;
use App\Libraries\Kangaroo\TransformableInterface;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\TransformerAbstract;
use ArrayIterator;
use Traversable;

/**
 * Implements the TransformableInterface in order to integrate "Fractal" transformers to Kangaroo
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Transformer implements TransformableInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param $item
     * @param TransformerAbstract $handler
     * @return mixed
     */
    public function transform($item, $handler)
    {
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $wrapper = $this->config->get('transformer.wrapper.item');

        $resource = new Item($item, $handler, $wrapper);

        return $manager->createData($resource)->toArray();
    }

    /**
     * Transforms a collection with or without the pagination
     *
     * @param $collection
     * @param TransformerAbstract $handler
     * @return array|Traversable
     */
    public function transformCollection($collection, $handler)
    {
        $manager = new Manager();

        $manager->setSerializer(new Serializer());

        $wrapper = $this->config->get('transformer.wrapper.collection');

        if($collection instanceof PaginationEntityInterface) {
            $resource = new Collection($this->prepareCollection($collection->getItems()), $handler, $wrapper);

            $data = $manager->createData($resource)->toArray();
            $pagination = $collection->getPagination();
            return [
                'items' => $data,
                'pagination' => [
                    'total_items' => $pagination->getTotalItems(),
                    'items_on_current_page' => $pagination->getItemsOnCurrentPage(),
                    'items_per_page_requested' => $pagination->getItemsPerPageRequested(),
                    'current_page' => $pagination->getCurrentPage(),
                    'total_pages' => $pagination->getTotalPages()
                ]
            ];
        } else {
            $resource = new Collection($this->prepareCollection($collection), $handler, $wrapper);

            if ($collection instanceof PaginationProviderInterface) {
                $resource->setPaginator(new Paginator($collection->getPagination()));
            }

            return $manager->createData($resource)->toArray();
        }


    }

    /**
     * Converts the collection to the array or ArrayIterator if it is an instance of something else
     *
     * @param $collection
     * @return array|ArrayIterator
     */
    private function prepareCollection($collection)
    {
        if (is_array($collection) || $collection instanceof ArrayIterator) {
            return $collection;
        }

        return iterator_to_array($collection);
    }
}