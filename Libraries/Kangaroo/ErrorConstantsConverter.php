<?php namespace App\Libraries\Kangaroo;

use App\Http\ErrorCodes\ErrorCodes;
use ReflectionClass;

class ErrorConstantsConverter
{

    private static $constants;

    public static function toJSONObject(){

        if(!self::$constants){
            $class = new ReflectionClass(ErrorCodes::class);
            self::$constants = json_encode($class->getConstants());
            return self::$constants;
        }
        return self::$constants;
    }
}