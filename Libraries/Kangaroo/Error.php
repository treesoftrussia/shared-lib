<?php
namespace App\Libraries\Kangaroo;

use App\Libraries\Kangaroo\Response\ResponseFactoryInterface;
use Illuminate\Http\Response;
use Illuminate\View\View;

/**
 * The class is used to output http errors.
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Error implements ErrorInterface
{

    const INVALID_SCOPES = 1;
    const INVALID_CREDENTIALS = 2;
    const VALIDATION_ERROR = 3;
    const PERMISSION_DENIED = 4;
    const ROUTE_NOT_FOUND = 5;
    const INVALID_CLIENT = 6;
    const UNKNOWN_ERROR = 7;
    const INTERNAL_ERROR = 8;
    const INVALID_GRANT_TYPE = 9;
    const METHOD_NOT_ALLOWED = 10;
    const TOO_MANY_REQUESTS = 11;

    const SMS_PUSHER_MANY_REQUESTS = 20;
    const INVALID_ACTIVATION_CODE = 21;
    
    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;
    
    /**
     * @param ResponseFactoryInterface $responseFactory
     */
    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param $data
     * @param $status
     * @param $httpCode
     * @return Response
     */
    public function write($data, $status, $httpCode)
    {
        if($data instanceof View){
            try{
                $data->render();
            } catch(\Exception $e){
                dd($e);
            }

            return new Response($data->render(), $httpCode, ['Content-Type' => 'text/html']);
        }

        $content = [
            'errorData' => $data,
            'errorCode' => $status
        ];

        return $this->responseFactory->create($content, $httpCode);
    }
}