<?php
namespace App\Libraries\Kangaroo;

use Illuminate\Support\ServiceProvider as Provider;
use App\Libraries\Kangaroo\Pagination\Paginator;
use App\Libraries\Kangaroo\Integrations\Transformer;
use App\Libraries\Kangaroo\Response\ResponseFactoryInterface;
use App\Libraries\Kangaroo\Response\JsonResponseFactory;
use Illuminate\Contracts\Debug\ExceptionHandler as ExceptionHandlerInterface;

/**
 * The service provider do the following:
 *  - binds implementations to the interfaces
 *  - registers ExceptionHandler
 *  - registers config provider
 *  - setups Paginator
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ServiceProvider extends Provider
{
    public function register()
    {
        $this->app->singleton(TransformableInterface::class, Transformer::class);
        $this->app->singleton(ResponseFactoryInterface::class, JsonResponseFactory::class);
        $this->app->singleton(ExceptionHandlerInterface::class, ExceptionHandler::class);

        $this->app->singleton(Config::class, function ($app) {
            return new Config($app['config']->get('kangaroo', []));
        });

        $this->app->singleton(ErrorInterface::class, Error::class);
    }

    public function boot()
    {
        Paginator::setConfig($this->app[Config::class]);
        Paginator::setRequest($this->app['request']);
    }
}