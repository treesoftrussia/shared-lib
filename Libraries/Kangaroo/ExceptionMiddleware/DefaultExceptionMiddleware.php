<?php

namespace App\Libraries\Kangaroo\ExceptionMiddleware;

use Closure;
use App\Libraries\Kangaroo\Error;
use App\Libraries\Kangaroo\Exceptions\InvalidInputException;
use App\Libraries\Kangaroo\Exceptions\ExceptionInterface;

class DefaultExceptionMiddleware
{

    private $error;

    public function __construct(Error $error)
    {
        $this->error = $error;
    }

    /**
     * @param $data
     * @param Closure $next
     * @return \Illuminate\Http\Response
     */
    public function handle($data, Closure $next)
    {
        if ($data->exception instanceof InvalidInputException) {
            return $this->error->write($data->exception->getErrors(), $data->exception->getInternalCode(), $data->exception->getHTTPCode());
        } else if ($data->exception instanceof ExceptionInterface) {
            return $this->error->write($data->exception->getData(), $data->exception->getInternalCode(), $data->exception->getHTTPCode());
        }
        
        return $next($data);
    }
}
