<?php
namespace App\Libraries\Kangaroo\Pagination;

use App\Libraries\Kangaroo\Config;
use Illuminate\Http\Request;

/**
 * Trait is used to take out from the Paginator class the methods used to Appect the thing from the environment.
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
trait AppectableTrait
{
    /**
     * @var Request;
     */
    private static $request;

    /**
     * @var Config
     */
    private static $config;

    /**
     * @param Request $request
     */
    public static function setRequest(Request $request)
    {
        static::$request = $request;
    }

    /**
     * @return Request
     */
    public static function getRequest()
    {
        return static::$request;
    }

    /**
     * @param Config $config
     */
    public static function setConfig(Config $config)
    {
        static::$config = $config;
    }

    /**
     * @return Config
     */
    public static function getConfig()
    {
        return static::$config;
    }
} 
