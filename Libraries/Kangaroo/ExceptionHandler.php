<?php
namespace App\Libraries\Kangaroo;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pipeline\Pipeline;
use Psr\Log\LoggerInterface;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ExceptionHandler extends Handler
{
    /**
     * @var Error
     */
    private $error;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var array
     */
    protected $dontReport = [
        'Symfony\Component\HttpKernel\Exception\HttpException'
    ];

    /**
     * @var
     */
    private $container;

    /**
     * ExceptionHandler constructor.
     * @param LoggerInterface $log
     * @param Repository $config
     * @param ErrorInterface $error
     * @param Container $container
     */
    public function __construct(LoggerInterface $log, Repository $config, ErrorInterface $error, Container $container)
    {
        parent::__construct($log);
        $this->error = $error;
        $this->container = $container;
        $this->config = $config;
    }

    /**
     * Renders an exception
     *
     * @param Request $request
     * @param Exception $e
     * @return Response
     */
    public function render($request, Exception $e)
    {

        $data = new \stdClass();
        $data->exception = $e;
        $data->request = $request;

        $error = $this->error;
        return (new Pipeline($this->container))
            ->send($data)
            ->through($this->config->get('app.exceptionHandlers'))
            ->then(function ($e) use ($error) {
                try {
                    return $error->write(method_exists($e->exception, 'getMessage') ? $e->exception->getMessage() : null, Error::UNKNOWN_ERROR, Response::HTTP_INTERNAL_SERVER_ERROR);
                } catch (Exception $e) {
                    dd($e);
                }
            });
    }
} 