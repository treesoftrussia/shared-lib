<?php
namespace App\Libraries\Kangaroo\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Libraries\Kangaroo\Error;
use Symfony\Component\HttpFoundation\Response;

/**
 * The exception is used to indicate that requested resource was not found.
 *
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
class ResourceNotFoundException extends NotFoundHttpException implements ExceptionInterface
{
    public function getHTTPCode(){
        return Response::HTTP_NOT_FOUND;
    }

    public function getInternalCode(){
        return Error::ROUTE_NOT_FOUND;
    }

    public function __construct($message = 'Resource not found error.')
    {
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}