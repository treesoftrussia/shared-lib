<?php
namespace App\Libraries\Kangaroo\Exceptions;

use \RuntimeException as DefaultRuntimeException;
use App\Libraries\Kangaroo\Error;
use Symfony\Component\HttpFoundation\Response;

/**
 * The exception is used to indicate that requested recourse was noit found.
 *
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
class RuntimeException extends DefaultRuntimeException implements ExceptionInterface
{

    public function getHTTPCode(){
        return Response::HTTP_INTERNAL_SERVER_ERROR;
    }

    public function getInternalCode(){
        return Error::INTERNAL_ERROR;
    }

    public function __construct($message = 'Internal server error.')
    {
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}