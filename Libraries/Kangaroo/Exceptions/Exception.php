<?php
namespace App\Libraries\Kangaroo\Exceptions;

use App\Http\ErrorCodes\ErrorCodes;
use App\Libraries\Kangaroo\ErrorDataFactory;
use Exception as DefaultException;
/**
 * The exception is used to indicate that access denied for requested resource.
 *
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
class Exception extends DefaultException implements ExceptionInterface
{
    private $HTTPCode;
    private $internalCode;
    private $data;

    public function getHTTPCode(){
        return $this->HTTPCode;
    }

    public function getInternalCode(){
        return $this->internalCode;
    }

    public function getData(){
        return isset($this->data) ? $this->data : $this->getMessage();
    }

    public function __construct($code)
    {
        $args = func_get_args();
        array_shift($args);

        $errorCode = new ErrorCodes($code);
        $errorData = ErrorDataFactory::make($errorCode, $args);

        $this->HTTPCode = $errorData->getHTTPCode();
        $this->internalCode = $code;
        //default code
        $this->code = $code;
        $this->data = $errorData->getData();
        parent::__construct(is_string($this->data) ? $this->data : '');
    }
} 