<?php
namespace App\Libraries\Kangaroo\Exceptions;


interface ExceptionInterface
{
    public function getHTTPCode();
    public function getInternalCode();
    public function getData();
}