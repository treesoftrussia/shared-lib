<?php
namespace App\Libraries\Kangaroo\Exceptions;

use App\Libraries\Kangaroo\Error;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * The exception is used to indicate that access denied for requested resource.
 *
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
class AccessDeniedException extends AccessDeniedHttpException implements ExceptionInterface
{

    public function getHTTPCode(){
        return Response::HTTP_UNAUTHORIZED;
    }

    public function getInternalCode(){
        return Error::PERMISSION_DENIED;
    }

    public function __construct()
    {
        parent::__construct('Access denied for requested resource.');
    }

    public function getData()
    {
        return $this->getMessage();
    }
}