<?php
namespace App\Libraries\Kangaroo\Exceptions;

use RuntimeException;
use App\Libraries\Kangaroo\Error;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class InvalidInputException extends RuntimeException implements ExceptionInterface
{
    public function getHTTPCode(){
        return Response::HTTP_BAD_REQUEST;
    }

    public function getInternalCode(){
        return Error::VALIDATION_ERROR;
    }

    /**
     * @var array
     */
    private $errors;

    public function __construct(array $errors)
    {
        parent::__construct('Invalid input error.');
        $this->errors = $errors;
    }

    public function getData()
    {
        return $this->getMessage();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
} 