

module.exports = function(bowerComponentsDir,basePath){
    basePath = basePath || "vendor/App/support";
    var fatlab = [basePath,"resources/fatlab/admin/template_content"].join('/');
    var resources = [basePath, "resources"].join("/");

    return {
        "img": [
        ],
        "fonts": [
        ],
        "css": [
            fatlab + "/css/bootstrap-reset.css",
            fatlab + "/css/style.css",
            fatlab + "/css/style-responsive.css",
        ],

        "js": [
            fatlab + "/js/common-scripts.js",
            resources + "/js/appBuilder.js"
        ],
        "app": [
            resources + "/js/module.js",
            resources + "/js/system/**/*"
        ]
    }
};