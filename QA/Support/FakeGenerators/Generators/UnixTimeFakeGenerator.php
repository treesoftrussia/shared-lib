<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace App\QA\Support\FakeGenerators\Generators;


use App\QA\Support\FakeGenerators\FakerGeneratorInterface;
use Faker\Generator;

class UnixTimeFakeGenerator implements FakerGeneratorInterface
{

    public function generate(Generator $faker)
    {
        return $faker->unixTime;
    }

}