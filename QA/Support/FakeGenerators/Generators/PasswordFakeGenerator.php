<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace App\QA\Support\FakeGenerators\Generators;

use App\QA\Support\FakeGenerators\FakerGeneratorInterface;
use Faker\Generator;

class PasswordFakeGenerator implements FakerGeneratorInterface
{

    public function generate(Generator $faker)
    {
        return htmlspecialchars($faker->password());
    }
}