<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
return [
    App\QA\Endpoints\Specification\CustomTypes\PhoneCustomType::class => \App\QA\Support\FakeGenerators\Generators\PhoneFakeGenerator::class,
    App\QA\Endpoints\Specification\CustomTypes\UnixTimeCustomType::class => \App\QA\Support\FakeGenerators\Generators\UnixTimeFakeGenerator::class,
    App\QA\Endpoints\Specification\CustomTypes\PasswordCustomType::class => \App\QA\Support\FakeGenerators\Generators\PasswordFakeGenerator::class,
    App\QA\Endpoints\Specification\CustomTypes\NameCustomType::class => \App\QA\Support\FakeGenerators\Generators\NameFakeGenerator::class,
    App\QA\Endpoints\Specification\CustomTypes\StatusCodeCustomType::class => \App\QA\Support\FakeGenerators\Generators\StatusCodeFakeGenerator::class,
    App\QA\Endpoints\Specification\CustomTypes\WordCustomType::class => \App\QA\Support\FakeGenerators\Generators\WordFakeGenerator::class,
];