<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace App\QA\Support\FakeGenerators;


use Faker\Generator;

interface FakerGeneratorInterface
{
    public function generate(Generator $faker);
}