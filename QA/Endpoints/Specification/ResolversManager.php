<?php
namespace App\QA\Endpoints\Specification;

use App\QA\Endpoints\Specification\Resolvers\AbstractResolver;
use App\QA\Endpoints\Specification\Resolvers\ArrayResolver;
use App\QA\Endpoints\Specification\Resolvers\CallableResolver;
use App\QA\Endpoints\Specification\Resolvers\ClassResolver;
use App\QA\Endpoints\Specification\Resolvers\CustomTypeResolver;
use App\QA\Endpoints\Specification\Resolvers\MultipleResolver;
use App\QA\Endpoints\Specification\Resolvers\ScalarResolver;
use App\QA\Endpoints\Specification\Resolvers\SimpleResolver;
use App\QA\Endpoints\Specification\Resolvers\ValueTypeResolver;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ResolversManager
{
    /**
     * @var AbstractSpecification
     */
    private $specification;

    /**
     * @var AbstractResolver[]
     */
    private $resolvers = [
        ArrayResolver::class,
        MultipleResolver::class,
        CallableResolver::class,
        ClassResolver::class,
        ValueTypeResolver::class,
        SimpleResolver::class,
        CustomTypeResolver::class,
        ScalarResolver::class,
    ];

    /**
     * @param AbstractSpecification $specification
     */
    public function __construct(AbstractSpecification $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @param $value
     * @param $attributes
     * @return AbstractResolver
     * @throws ResolverException
     */
    public function createResolver($value, $attributes)
    {
        foreach ($this->resolvers as $resolver) {
            if ($resolver::canResolve($value, $attributes)) {
                return new $resolver($this, $value, $attributes);
            }
        }

        throw new ResolverException('Cannot resolve specification');
    }
}