<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace App\QA\Endpoints\Specification\CustomTypes;


class UnixTimeCustomType extends AbstractCustomType
{

    /**
     * @return string
     */
    public function getValidationString()
    {
        return 'digits_between:5,12';
    }
}