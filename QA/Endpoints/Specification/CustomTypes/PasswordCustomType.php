<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace App\QA\Endpoints\Specification\CustomTypes;

class PasswordCustomType extends AbstractCustomType
{

    /**
     * @return string
     */
    public function getValidationString()
    {
        return 'string';
    }
}