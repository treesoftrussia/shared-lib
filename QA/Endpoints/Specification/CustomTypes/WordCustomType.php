<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace App\QA\Endpoints\Specification\CustomTypes;

class WordCustomType extends AbstractCustomType
{
    /**
     * @return string
     */
    public function getValidationString()
    {
        return 'string';
    }

}