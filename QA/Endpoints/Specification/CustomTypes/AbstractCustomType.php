<?php
namespace App\QA\Endpoints\Specification\CustomTypes;

/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
abstract class AbstractCustomType
{
    /**
     * @return string
     */
    abstract public function getValidationString();
}