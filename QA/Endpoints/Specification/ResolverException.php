<?php
namespace App\QA\Endpoints\Specification;

use Exception;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ResolverException extends Exception
{

}