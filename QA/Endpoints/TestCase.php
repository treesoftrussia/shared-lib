<?php
namespace App\QA\Endpoints;

use App\QA\Endpoints\Entity\AbstractEntity;
use App\QA\Endpoints\Specification\AbstractSpecification;
use App\QA\Endpoints\Validator\CollectionSummary;
use App\QA\Endpoints\Validator\RulesProviderInterface;
use App\QA\Support\JsonRequest;
use App\QA\Endpoints\Validator\ValidatorConstraint;
use Illuminate\Database\Connection;
use Illuminate\Foundation\Application as ApplicationInstance;
use Illuminate\Foundation\Testing\TestCase as IlluminateTestCase;
use Illuminate\Http\Response;
use Mockery\CountValidator\Exception;
use RuntimeException;
use PHPUnit_Framework_Assert as PHPUnit;
use Faker\Generator;
use Illuminate\Contracts\Http\Kernel;
use App\QA\Endpoints\Faker\FakerFactoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestCase extends IlluminateTestCase
{
    /**
     * @var AbstractEntity
     */
    protected $entity;

    /**
     * @var Generator
     */
    protected $faker;

    /**
     * @var ApplicationInstance
     */
    protected $http;

    /**
     * @var ApplicationInstance
     */
    private static $cachedApp;

    /**
     * @var array
     */
    private static $isTablesTruncated = [];

    /**
     * Set FOREIGN_KEY_CHESKS directive. Used for database truncate function
     *
     * @var boolean
     */
    protected $foreignKeyChecks = true;

    /**
     * @var array
     */
    protected $presetHeaders = [];

    /**
     * @return ApplicationInstance
     */
    public function createApplication()
    {
        if (self::$cachedApp === null){
            self::$cachedApp = Application::test(function($app){
                $app->make(DbManager::class)->change();
            });
        }

        return self::$cachedApp;
    }

    public function assertEntity($requestData, $idOrData, $entityClass, $constraintClass, $message = ''){
        if(is_array($idOrData)){
            $entity = $idOrData;
        } else {
            $entity = $this->app->make($entityClass)->newQuery()->find($idOrData)->toArray();
        }

        $constraintClass = new $constraintClass($requestData);

        PHPUnit::assertThat($entity, $constraintClass, $message);
    }

    /**
     * Asserts structure of the response
     *
     * @param array $response
     * @param AbstractSpecification $specification
     */
    protected function assertStructure(
        array $response,
        AbstractSpecification $specification
    )
    {
        if (!isset($response['data'])) {
            $response = [$response];
        } else {
            $response = $response['data'];
        }

        foreach ($response as $key => $item) {
            $result = $specification->check($item);
            $this->assertTrue($result->isOk(),
                "Structure is invalid for '{$key}' element:" . $result->getMessage());
        }
    }

    /**
     * @param RulesProviderInterface $rules
     * @param string $message
     * @throws
     */
    protected function assertValidator(RulesProviderInterface $rules, $message = '')
    {
        $summary = $this->http->make(CollectionSummary::class)->get();
        $rules = $rules->rules();

        if (!$rules) {
            throw new RuntimeException("Rules shouldn't be empty.");
        }

        $constraint = new ValidatorConstraint($summary);
        PHPUnit::assertThat($rules, $constraint, $message);
    }

    /**
     * @param $method
     * @param $uri
     * @param array $parameters
     * @param array $options
     * @param array $additionalHeaders
     * @return mixed
     */
    protected function request($method, $uri, $parameters = [], $options = [], $additionalHeaders = [])
    {
        $additionalHeaders = array_merge($this->presetHeaders, $additionalHeaders);
        $response = $this->shoot($method, $uri, $parameters, $options, $additionalHeaders);
        return json_decode($response->getContent(), true);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $parameters
     * @param array $options
     * @param array $additionalHeaders
     * @return  Response
     */
    public function shoot($method, $uri, $parameters = [], $options = [], $additionalHeaders = [])
    {
        $this->http = Application::http(function($app){
            $app->make(DbManager::class)->change();
        });

        $request = JsonRequest::make($uri, $method, $parameters, $options, $additionalHeaders);

        $this->response = $this->http->make(Kernel::class)->handle($request);
        $this->http->make(DbManager::class)->getConnection()->disconnect();
        return $this->response;
    }

    /**
     * @param string $uri
     * @param array $parameters
     * @param array $options
     * @param array $additionalHeaders
     * @return array
     */
    public function get($uri, array $parameters = [], array $options = [], array $additionalHeaders = [])
    {
        return $this->request('GET', $uri, $parameters, $options, $additionalHeaders);
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $options
     * @param array $additionalHeaders
     * @return array
     */
    public function post($uri, array $data = [], array $options = [], array $additionalHeaders = [])
    {
        return $this->request('POST', $uri, $data, $options, $additionalHeaders);
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $options
     * @param array $additionalHeaders
     * @return array
     */
    public function put($uri, array $data = [], array $options = [], array $additionalHeaders = [])
    {
        return $this->request('PUT', $uri, $data, $options, $additionalHeaders);
    }

    /**
     * @param string $uri
     * @param array $data
     * @param array $options
     * @param array $additionalHeaders
     * @return array
     */
    public function patch($uri, array $data = [], array $options = [], array $additionalHeaders = [])
    {
        return $this->request('PATCH', $uri, $data, $options, $additionalHeaders);
    }

    /**
     * @param string $uri
     * @param array $parameters
     * @param array $options
     * @param array $additionalHeaders
     * @return array
     */
    public function delete($uri, array $parameters = [], array $options = [], array $additionalHeaders = [])
    {
        return $this->request('DELETE', $uri, $parameters, $options, $additionalHeaders);
    }

    /**
     * Overrides to provide a hook for before test logic execution
     */
    public function setUp()
    {
        parent::setUp();

        //if (!isset(self::$isTablesTruncated[get_called_class()])){
        $this->truncateAllTables();
        //    self::$isTablesTruncated[get_called_class()] = true;
        //}

        $this->beforeTest();
    }

    /**
     * Overrides to provide a hook for after test logic execution
     */
    public function tearDown()
    {
        if(!$this->foreign_key_checks) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        $this->afterTest();
        $this->entity->close();

        if(!$this->foreign_key_checks) {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }


    /**
     * Executed after the test method is completed
     */
    protected function afterTest()
    {
        //
        $this->truncateAllTables();
    }

    /**
     * Executed before the test method is started
     */
    protected function beforeTest()
    {
        $this->entity = $this->app->make(AbstractEntity::class);
        $this->faker = $this->app->make(FakerFactoryInterface::class)->create();
    }

    private function truncateAllTables()
    {
        /**
         * @var Connection $connection
         */
        $connection = $this->app->make(DbManager::class)->getConnection();

        $tables = $connection->getDoctrineSchemaManager()->listTableNames();

        if(!$this->foreign_key_checks) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        foreach ($tables as $table){
            DB::statement("TRUNCATE TABLE ".$table);
        }

        if(!$this->foreign_key_checks) {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

    }
}