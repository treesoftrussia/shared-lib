<?php
namespace App\QA\Endpoints\Faker;

interface FakerFactoryInterface
{
    public function create();
}
