<?php
namespace App\QA\Endpoints\Validator;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface RulesProviderInterface
{
    /**
     * @return array
     */
    public function rules();
} 