<?php
/**
 * These are just shortcuts for the methods of "Debug" class. Please see it for more details.
 */
function pre()
{
    call_user_func_array('App\Libraries\Debug::pre', func_get_args());
}

function pred()
{
    call_user_func_array('App\Libraries\Debug::pred', func_get_args());
}

function vred()
{
    call_user_func_array('App\Libraries\Debug::vred', func_get_args());
}

function vre()
{
    call_user_func_array('App\Libraries\Debug::vre', func_get_args());
}
